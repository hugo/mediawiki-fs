MediaWiki Filesystem
====================

**THIS PROJECT IS ABANDONED**

I instead recommend Git's extension for MediaWiki remotes
(https://github.com/Git-Mediawiki/Git-Mediawiki), which is also included in the
upstream git repo.

--------------------------------------------------


Here are three separate attempts to mount a MediaWiki page as a filesystem under
fuse. (https://datorhandbok.lysator.liu.se to be exact).

Versions
--------

All three versions work, but with the following problems:

### C++

Fuse works great, but fetching and decoding data is clumsy in C++ to say the
least.

### Python

The fuse library i tried to use *required* python that all callbacks returned
python strings instead of byte-vectors. It then took the length of the string
(?) instead of the length in bytes, breaking just about everything.

### (Chicken) Scheme

The most working version. Sensible fuse bindings, C-style strings. Downloading
and parsing of XML works wonders. However...

XML & Json
----------

MediaWiki has for some reason deprecated the XML api, and wants everyone to use
their json api. This would be fine, except that the actual wiki-page is still
presented as XML. The XML is also far from satisfactory. Instead of nesting the
document through namespaces the inner is HTML-encoded (`&lt;` for every `<`).

Design problems
---------------
This prototype work, and getting a fully featured read-only version wouldn't be
to far away. One big problem is however that MediaWiki isn't inherently
hierarchical, making the mapping to a filesystem a bit harder. These current
versions make some attempt to fetch indices, but mostly fall back to showing
those pages it knows exists, from the user explicitly opening a (as of then)
non-existant file.
