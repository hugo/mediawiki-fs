LIBS = $(shell pkg-config --libs libcurl fuse tinyxml2)
CFLAGS = $(shell pkg-config --cflags libcurl fuse tinyxml2) -ggdb \
		 -std=c++2a -Wall -Wextra -pedantic

CPP_FILES = $(wildcard *.cpp)
O_FILES = $(CPP_FILES:%.cpp=obj/%.o)

$(shell mkdir -p obj/)

obj/%.o: %.cpp
	$(CXX) -c $(CFLAGS) -o $@ $<

mfs: $(O_FILES)
	$(CXX) -o $@ $^ $(LIBS)
