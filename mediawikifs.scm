(import (chicken process signal)
        (chicken process-context)
        (chicken process-context posix)

        (chicken string)
        (chicken bitwise)

        (srfi 1)
        (srfi 69)                       ; hash tables

        fuse

        ssax
        sxpath
        sxpath-lolevel                  ; for sxml:text
        http-client
        uri-common)

(define-syntax let*
  (syntax-rules ()

    ;; Base case
    [(_ () body ...)
     (begin body ...)]

    ;; "Regular" case
    [(_ ((k value) rest ...) body ...)
     (let ((k value))
       (let* (rest ...)
         body ...))]

    ;; SRFI-71 let-values
    [(_ ((k k* ... values) rest ...) body ...)
     (call-with-values (lambda () values)
       (lambda (k k* ...)
         (let* (rest ...)
           body ...)))]

    ;; Declare variable without a value (actuall #f).
    ;; Useful for inner mutation.
    [(_ (v rest ...) body ...)
     (let* ((v #f) rest ...) body ...)]
    ))



(set! (form-urlencoded-separator) "&;")



(define (get-xml-page url)
 (call-with-input-request
  url #f
  (lambda (port) (ssax:xml->sxml port '((mw . "http://www.mediawiki.org/xml/export-0.10/"))))))







(define base-uri
  (make-uri
   scheme: 'https
   host: "datorhandbok.lysator.liu.se"
   path: '(/ "api.php")))

(define (category-uri #!optional continue-from)
 (update-uri
  base-uri
  query: `((action . query)
           (list . allcategories)
           (format . xml)
           ,@(if continue-from
                 `((accontinue . ,continue-from))
                 '()))))


;; returns string list of all categories
(define (all-categories #!optional continue-from)
  (let* ((page _ _ (get-xml-page (category-uri continue-from))))
    (append (map sxml:text ((sxpath '(// allcategories c)) page))
            (let ((continue ((if-sxpath '(// @ accontinue)) page)))
              (if continue
                  (all-categories (sxml:text continue))
                  '())))))

(define (category-members-uri category-name #!optional continue-from)
 (update-uri
  base-uri
  query: `((action . query)
           (list . categorymembers)
           (cmtitle . ,(string-append "Category:" category-name))
           (format . xml)
           ,@(if continue-from
                 `((cmcontinue . ,continue-from))
                 '()))))

;; Get string list of pages belonging to category
(define (all-pages-in-category category-name #!optional continue-from)
  (let* ((page _ _ (get-xml-page (category-members-uri
                                  category-name continue-from))))
    (append (map sxml:text ((sxpath '(// categorymembers cm @ title)) page))
            (let ((continue ((if-sxpath '(// @ cmcontinue)) page)))
              (if continue
                  (all-pages-in-category category-name (sxml:text continue))
                  '())))))


(define (uri-for-page page)
 (update-uri base-uri
             query: `((action . query)
                      (export . #t)
                      (exportnowrap . #t)
                      (titles . ,page))))

(define (get-page ht pagename)
  (unless (hash-table-exists? ht pagename)
    (let* ((sxml _ _ (get-xml-page (uri-for-page pagename))))
      (when ((if-sxpath '(// mw:page)) sxml)
        (set! (hash-table-ref ht pagename) sxml))))
  (hash-table-ref/default ht pagename #f))



(define categories (alist->hash-table
                    (map (cut cons <> 'not-downloaded)
                         (all-categories))))

;; wrapper around `all-pages-in-category', which also updates hash-map
(define (category-page-names category)
  (case (hash-table-ref/default categories category 'no-such-page)
    ((no-such-page) #f)
    ((not-downloaded)
     (let ((pages (all-pages-in-category category)))
       (set! (hash-table-ref categories category) pages)
       pages))
    (else => identity)))

(define pages (make-hash-table))

;; Get id field of page object
(define (page-id page)
  (let ((id-str (sxml:text ((sxpath '(// mw:page mw:id)) page))))
    (read (open-input-string id-str))))

;; Get byte length field of page object
(define (page-length page)
  (let ((length-str (sxml:text ((sxpath '(// mw:page // mw:text @ bytes)) page))))
    (read (open-input-string length-str))))


#|
 | /
 |   /<category>
 |     /<pages>
 |#

(define mediawiki-fs
  (make-filesystem
   init: (lambda () (print "Filesystem ready"))

   readdir: (lambda (path-str)
              (let ((path (string-split path-str "/")))
               (cons* "." ".."
                      (map ->string
                       (cond [(null? path) (hash-table-keys categories)]
                             [(= 1 (length path)) (category-page-names (car path))]
                             [else '()])))))

   getattr: (lambda (path-str)
              (print path-str)
              (let ((path (string-split path-str "/")))
                (cond
                 ((null? path)
                  (vector (bitwise-ior file/dir #o555)
                          2
                          (current-user-id)
                          (current-group-id)
                          (hash-table-size categories)
                          0 0 0))

                 ((= 1 (length path))
                  (let ((sub-pages (category-page-names (car path))))
                    (and sub-pages
                         (vector (bitwise-ior file/dir #o555)
                                 2
                                 (current-user-id)
                                 (current-group-id)
                                 (length sub-pages)
                                 0 0 0))))

                 (else
                  (let* ((page (get-page pages (cadr path))))
                    (and page
                         (vector (bitwise-ior file/reg #o444)
                                 1 ; TODO change with how many categories it belongs to
                                 (current-user-id) (page-id page)
                                 (page-length page)
                                 0 0 0)))))))

   open: (lambda (path-str mode)
           (let ((path (string-split path-str "/")))
             (cond ((> 2 (length path)) #f)
                   (else (get-page pages (cadr path))))))

   read: (lambda (handle size offset)
           ;; TODO handle size and offset
           (sxml:text
            ((sxpath '(// mw:page // mw:text)) handle)))
   ))

#;
(filesystem-start! "mnt" mediawiki-fs)

(define path (car (command-line-arguments)))

(when (filesystem-start! path mediawiki-fs)
  (print "Starting filesystem")
  (set-signal-handler!
    signal/int
    (lambda _ (filesystem-stop! path mediawiki-fs)))
  (filesystem-wait! path mediawiki-fs)
  (print "Filesystem shut down"))
