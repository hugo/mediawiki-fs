#define FUSE_USE_VERSION 28

#include <string>
#include <map>

#include <sys/types.h>
#include <fuse.h>

#define url "https://datorhandbok.lysator.liu.se/index.php/Special:Exportera/"

#define ERR(s, ...) fprintf(stderr, "\x1b[0;31mERR\x1b[m " s "\n", ## __VA_ARGS__)

struct page {
	bool init = false;
	std::string name;
	size_t size;
	time_t timestamp;
	std::string data;
};

extern "C" {
	int getattr_callback(const char*, struct stat*);
	int open_callback (const char*, struct fuse_file_info*);
	int read_callback (const char*, char*, size_t, off_t, struct fuse_file_info*);
	int readdir_callback (const char*, void*, fuse_fill_dir_t, off_t, struct fuse_file_info*);
}

size_t actual_size (std::map<std::string, page>& pages);
