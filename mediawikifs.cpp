#include "mediawikifs.h"

#include <map>
#include <string>
#include <iostream>

#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <ctime>

#include <sys/types.h>
#include <sys/stat.h>
#include <curl/curl.h>
#include <fuse.h>

#include <tinyxml2.h>

static std::string str;

static CURL* curl;
static std::map<std::string, page> pages;

time_t parse_timestring (const char* str_) {
	char* str = new char[strlen(str_)];
	strcpy(str, str_);

	int len = 1;
	while (*(++str) != '\0') ++len;

	int year, month, day, hour, minute, second, zone_offset;

	switch (len) {
		case 20:
			zone_offset = 0;
			--str; /* Z */
			/* fallthrough */
		case 19:
			second  =  *(--str) - '0';
			second += (*(--str) - '0') * 10;
			--str; /* : */
			minute  =  *(--str) - '0';
			minute += (*(--str) - '0') * 10;
			--str; /* : */
			hour    =  *(--str) - '0';
			hour   += (*(--str) - '0') * 10;
			--str; /* T */
			/* fallthrough */
		case 10:
			day     =  *(--str) - '0';
			day    += (*(--str) - '0') * 10;
			--str; /* - */
			month   =  *(--str) - '0';
			month  += (*(--str) - '0') * 10;
			--str; /* - */
			year    =  *(--str) - '0';
			year   += (*(--str) - '0') * 10;
			year   += (*(--str) - '0') * 100;
			year   += (*(--str) - '0') * 1000;
	}
	delete str;

	struct tm t;
	t.tm_sec = second;
	t.tm_min = minute;
	t.tm_hour = hour;
	t.tm_mday = day;
	t.tm_mon = month - 1;
	t.tm_year = year - 1900;

	return mktime (& t);
}

using namespace tinyxml2;

page* get_page(const char* path) {
	page* p = & pages[path];

	if (p->init) return p;

	str = "";
	CURLcode code;
	std::string urlstring = url;
	urlstring += path;
	code = curl_easy_setopt (curl, CURLOPT_URL, urlstring.c_str());
	if (code != CURLE_OK) ERR("Set url");
	code = curl_easy_perform (curl);
	if (code != CURLE_OK) ERR("Perform %i", code);

	XMLDocument doc;
	doc.Parse (str.c_str());
	XMLNode* root = doc.RootElement();
	if (root == nullptr) return nullptr;

	XMLNode* page = root->FirstChildElement("page");
	if (page == nullptr) return nullptr;

	XMLNode* revision = page->FirstChildElement("revision");
	if (revision == nullptr) return nullptr;

	const char* timestr =
		revision->FirstChildElement("timestamp")
		        ->ToElement()
		        ->GetText();


	p->init = true;
	p->name = path;
	p->timestamp = parse_timestring (timestr);

	XMLElement* el = revision->FirstChildElement("text")->ToElement();

	p->data = el->GetText();
	p->data += "\n";
	p->size = el->UnsignedAttribute("bytes") + 1;

	return p;
}

size_t handle_data (void* buffer, size_t, size_t nmemb, void*) {
	str += (char*) buffer;
	return nmemb;
}

int getattr_callback(const char* path, struct stat* st) {
	if (strcmp(path, "/") == 0) {

		st->st_mode = S_IFDIR | 0555;
		st->st_size = actual_size(pages);

	} else {

		page* p = get_page(path + 1);
		if (p == nullptr) {
			if (p == nullptr) return -ENOENT;
		}

		st->st_mode = S_IFREG | 0444;
		st->st_size = p->size;
		st->st_mtime = p->timestamp;
	}

	return 0;
}

int open_callback (const char* /*path*/, struct fuse_file_info* /*f*/) {
	return 0;
}

int read_callback (
		const char* path,
		char* buf,
		size_t size,
		off_t offset,
		struct fuse_file_info* /*f*/)
{
	page* p = get_page (path + 1);

	if (p == nullptr) return -ENOENT;

	std::string* s = & p->data;
	ssize_t len = s->size();
	
	if (offset >= len) return 0;

	if (offset + (off_t) size > len) {
		size = len - offset;
	}

	*s = s->substr(offset, offset + size);

	memcpy (buf, s->c_str(), size);
	return s->size();
}

int readdir_callback (
		const char* path,
		void* buf,
		fuse_fill_dir_t filler,
		off_t /* offset */,
		struct fuse_file_info* /*f*/)
{
	if (strcmp(path, "/") != 0) {
		return -ENOMEM;
	}

	filler(buf, ".", NULL, 0);
	filler(buf, "..", NULL, 0);
	// filler(buf, "Tottaly a file", NULL, 0);

	for (auto kv : pages) {
		if (! kv.second.init) continue;
		filler(buf, kv.first.c_str(), NULL, 0);
	}

	return 0;
}

size_t actual_size (std::map<std::string, page>& pages) {
	size_t size = 0;

	for (auto kv : pages) {
		if (! kv.second.init) continue;
		++size;
	}

	return size;

}

/*
 * Calls when file system gets mounted
 */
void* init_callback (struct fuse_conn_info* /*conn*/) {
	CURLcode code;
	code = curl_global_init (CURL_GLOBAL_DEFAULT);
	if (code != CURLE_OK) ERR("Global init");

	curl = curl_easy_init ();
	if (curl == NULL) ERR("curl");

	code = curl_easy_setopt (curl, CURLOPT_WRITEFUNCTION, handle_data);
	if (code != CURLE_OK) ERR("Write data");

	return NULL;
}

/*
 * Called on filesystem exit
 */
void destroy_callback (void*) {
	curl_easy_cleanup (curl);
}

struct fuse_operations fops;

int main (int argc, char* argv[]) {
	fops.getattr = getattr_callback;
	fops.open    = open_callback;
	fops.read    = read_callback;
	fops.readdir = readdir_callback;
	fops.init    = init_callback;
	fops.destroy = destroy_callback;

	return fuse_main (argc, argv, &fops, NULL);
}
